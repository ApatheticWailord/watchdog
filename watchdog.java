import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class watchdog {

    public static void main(String[] args) throws IOException {

        String path = "%PROGRAMFILES%\\Mozilla Firefox\\firefox.exe";

        List<MatchResult> results = Pattern.compile("%(?<name>[^%]+)%").matcher(path).results().toList();
        for(MatchResult p : results) path = path.replace(p.group(), System.getenv(p.group()));

        String finalPath = path;
        boolean isProcess = ProcessHandle
                .allProcesses()
                .anyMatch(
                        p -> p.info()
                                .command()
                                .orElse("")
                                .endsWith(
                                        Paths.get(finalPath).getFileName().toString()));
        if(!isProcess){
            File file = new File(path);

            if(!file.exists()){
                throw new FileNotFoundException(path);
            }

            new ProcessBuilder(path).start();
        }


    }
}
